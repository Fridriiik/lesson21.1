
#include <iostream>

class  IA {
public:
	IA() {
		std::cout << "A constructed\n";
	}

	virtual ~IA() {
		//std::cout << "A destroyed\n";
	}

	virtual void Show() {
		std::cout << "A";
	}

	virtual IA* GetSelf(){
		return this;
	}

	virtual void ShowInfo() = 0;
};

void IA::ShowInfo() {
	std::cout << "default implementation \n";
}

class B : virtual public IA {
public:
	B() {
		std::cout << "B constructed\n";
	}
	~B() {
		//std::cout << "B destroyed\n";
	}
	void Show() override {
		std::cout << "B";
	}

	B* GetSelf() override {
		return this;
	}

	void Test() {
		std::cout << "Only in class B";
	}

	virtual void ShowInfo() override {
		IA::ShowInfo();
		std::cout << "Show info in class B \n";
	}
};

class C : virtual public IA {
public:
	C() {
		std::cout << "C constructed\n";
	}
	~C() {}

	virtual void ShowInfo() override {
		IA:ShowInfo();
	}
};

class D : virtual public B, virtual public C {
public:
	D() {
		std::cout << "D constructed \n";
	}

	virtual void ShowInfo() override {
		IA:ShowInfo();
	}
};

int main() {
	/*
	A* p = new B;
	auto newPointer = static_cast<B*>(p->GetSelf());
	newPointer->Test();
	auto anotherNewPtr = newPointer->GetSelf();
	anotherNewPtr->Test();
	*/
	D* a = new D;

	delete a;
	//std::cout << "we destroyed a\n";
}